package br.edu.unifei.bi20.kpi;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import br.edu.unifei.bi20.R;

import org.w3c.dom.Text;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Iterator;

/**
 * Created by Bruno on 9/1/2015.
 */
public class KPIAdapter extends ArrayAdapter<KPIEntry>{

    Context context;
    int layoutResourceId;
    ArrayList<KPIEntry> data = null;
    public final static int MONTH = Calendar.getInstance().get(Calendar.MONTH);

    public KPIAdapter(Context context, int layoutResourceId, ArrayList<KPIEntry> data) {
        super(context, layoutResourceId, data);
        this.layoutResourceId = layoutResourceId;
        this.context = context;
        this.data = data;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View row = convertView;
        WeatherHolder holder = null;

        if (row == null)
        {
            LayoutInflater inflater = ((Activity)context).getLayoutInflater();
            row = inflater.inflate(R.layout.gridview_item, parent, false); //Trocar por listview_item para visao com lista

            holder = new WeatherHolder();
            holder.txtTitle = (TextView)row.findViewById(R.id.txtTitle);
            if(holder.txtTitle == null) System.out.println("NULL HERE1");
            holder.imgIcon = (ImageView)row.findViewById(R.id.imgIcon);
            holder.imgStatus = (ImageView)row.findViewById(R.id.imgStatus);
            holder.txtMonth = (TextView)row.findViewById(R.id.kpival);

            row.setTag(holder);
        }
        else {
            holder = (WeatherHolder)row.getTag();
        }

        KPIEntry e = data.get(position);
        KPI m = e.kpi;
        Double dval = e.getValue();
        String sval = String.valueOf(dval);
        if(m.isPorcentage()){
           // dval*=100;
            sval = String.valueOf(dval*100)+ "%";
        }
        String value = dval==-1f?"Not published yet":"Month: "+sval;

        holder.txtTitle.setText(m.getName_kpi());
        holder.imgIcon.setImageResource(m.getIcon());
        holder.imgStatus.setImageResource(m.getStatus());
        holder.txtMonth.setText(value);
       // holder.imgIcon.setBackgroundResource(0);

        return row;
    }

   static class WeatherHolder
    {
        public ImageView imgIcon;
        public TextView txtTitle;
        public ImageView imgStatus;
        public TextView txtMonth;
    }

}
