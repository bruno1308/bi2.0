package br.edu.unifei.bi20;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.CheckedTextView;
import android.widget.ImageView;

import br.edu.unifei.bi20.kpi.KPI;

import java.util.ArrayList;

/**
 * Created by Bruno on 9/3/2015.
 */
public class FavoritesAdapter extends ArrayAdapter<KPI> {
    Context context;
    int layoutResourceId;
    ArrayList<KPI> data = null;

    public FavoritesAdapter(Context context, int layoutResourceId, ArrayList<KPI> data) {
        super(context, layoutResourceId, data);
        this.layoutResourceId = layoutResourceId;
        this.context = context;
        this.data = data;
    }


    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View row = convertView;
        WeatherHolder holder = null;

        if (row == null)
        {
            LayoutInflater inflater = ((Activity)context).getLayoutInflater();
            row = inflater.inflate(R.layout.listview_settings, parent, false);

            holder = new WeatherHolder();
            holder.imgIcon = (ImageView)row.findViewById(R.id.imgIcon);
            holder.chk = (CheckedTextView)row.findViewById(R.id.chkText1);

            row.setTag(holder);
        }
        else {
            holder = (WeatherHolder)row.getTag();
        }

        KPI m = data.get(position);

        holder.chk.setText(m.getName_kpi());
        holder.imgIcon.setImageResource(m.getIcon());
       holder.chk.setChecked(m.isFavorite());

        return row;
    }

    static class WeatherHolder
    {
        public ImageView imgIcon;

        public CheckedTextView chk;
    }
}
