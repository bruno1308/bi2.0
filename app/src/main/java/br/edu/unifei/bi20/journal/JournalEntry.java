package br.edu.unifei.bi20.journal;

import java.util.Date;

/**
 * Created by Bruno on 9/4/2015.
 */
public class JournalEntry {

    public int getId_entry() {
        return id_entry;
    }

    public void setId_entry(int id_entry) {
        this.id_entry = id_entry;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    private int id_entry;
    private String title, desc,owner,latest;
    private String category;

    public Date getLast_date() {
        return last_date;
    }

    public void setLast_date(Date last_date) {
        this.last_date = last_date;
    }

    private Date last_date;
    public JournalEntry(String c, String o, String t, String d, String l, Date dt){
        category = c;
        owner = o;
        latest = l;
        title = t;
        desc = d;
        last_date = dt;
    }

    public String getOwner() {
        return owner;
    }

    public void setOwner(String owner) {
        this.owner = owner;
    }

    public String getLatest() {
        return latest;
    }

    public void setLatest(String latest) {
        this.latest = latest;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String subject) {
        this.category = subject;
    }
}
