package br.edu.unifei.bi20.utils;

import android.content.Context;

import org.json.JSONObject;

import java.text.ParseException;
import java.util.ArrayList;

/**
 * Created by Bruno on 10/6/2015.
 */
public abstract class JSONToObject<T> {
    abstract  ArrayList<T> processJSON(JSONObject object, Context c,String kpiname) throws ParseException;
}
