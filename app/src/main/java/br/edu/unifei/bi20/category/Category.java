package br.edu.unifei.bi20.category;

/**
 * Created by Bruno on 05/09/2015.
 */
public class Category {
    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    private String nome;
    private boolean isFavorite;
    public Category(String d){
        nome = d;
    }

    public boolean isFavorite() {
        return isFavorite;
    }

    public void setIsFavorite(boolean isFavorite) {
        this.isFavorite = isFavorite;
    }
}
