package br.edu.unifei.bi20;

import android.content.Context;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.CheckedTextView;
import android.widget.ListView;

import br.edu.unifei.bi20.kpi.KPI;
import br.edu.unifei.bi20.utils.DatabaseHelper;

import java.util.ArrayList;

public class FavoritesActivity extends AppCompatActivity {
    private ListView listView1;
    SharedPreferences sp;
    ArrayList<KPI> metrica_data;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_favorites);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar_favorites);
        setSupportActionBar(toolbar);
        toolbar.inflateMenu(R.menu.menu_favorites);
        sp = this.getSharedPreferences(DatabaseHelper.PREFS_NAME,
                Context.MODE_PRIVATE);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        toolbar.setNavigationIcon(R.drawable.abc_ic_ab_back_mtrl_am_alpha);


        metrica_data = new ArrayList<>();

        metrica_data.add(new KPI(R.mipmap.lwdc, "Last Work Day Case", R.mipmap.greendot));
        metrica_data.add(new KPI(R.mipmap.lifr, "Line Item Fill Rate", R.mipmap.greendot));
        metrica_data.add(new KPI(R.mipmap.backorder, "Back Order", R.mipmap.reddot));
        metrica_data.add( new KPI(R.mipmap.devextension, "Deviation with Extension", R.mipmap.greendot));
        metrica_data.add( new KPI(R.mipmap.devextension, "Deviation without Extension", R.mipmap.yellowdot));
        metrica_data.add( new KPI(R.mipmap.scheduleatt, "Schedule Attainment", R.mipmap.greendot));
        metrica_data.add(  new KPI(R.mipmap.scheduleatt, "On Time Release", R.mipmap.greendot));
        metrica_data.add( new KPI(R.mipmap.dfrft, "Deviation Free Right First Time", R.mipmap.greendot));
        metrica_data.add(new KPI(R.mipmap.volume, "Volume", R.mipmap.greendot));
        metrica_data.add(new KPI(R.mipmap.volume, "Days of Supply", R.mipmap.greendot));
        metrica_data.add(new KPI(R.mipmap.cost, "Variable Cost", R.mipmap.yellowdot));
        metrica_data.add(new KPI(R.mipmap.cost, "Total Cost", R.mipmap.reddot));

        for(KPI k:metrica_data){
            boolean b = sp.getBoolean(k.getName_kpi(),true);
            k.setIsFavorite(b);
        }

        final FavoritesAdapter myadapter = new FavoritesAdapter(this, R.layout.listview_settings,metrica_data);
        listView1 = (ListView)findViewById(R.id.lstFavorites);
        //   LayoutInflater inflater = getLayoutInflater();
        //   View header = inflater.inflate(R.layout.header, listView1, false);
        // listView1.addHeaderView(header, null, false);
        listView1.setAdapter(myadapter);

        listView1.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> arg0, View arg1, int arg2,
                                    long arg3) {
                CheckedTextView checkedTextView = (CheckedTextView) arg1.findViewById(R.id.chkText1);
                checkedTextView.toggle();
                //String key = metrica_data[arg2].name_kpi;
                metrica_data.get(arg2).setIsFavorite(checkedTextView.isChecked());
                SharedPreferences.Editor editor = sp.edit();

                editor.putBoolean(metrica_data.get(arg2).getName_kpi(), metrica_data.get(arg2).isFavorite());
                editor.commit();
                myadapter.notifyDataSetChanged();
                //listView1.setItemChecked(arg2, false);

            }
        });


    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_favorites, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
