package br.edu.unifei.bi20.utils;

import android.content.Context;

import br.edu.unifei.bi20.journal.JournalEntry;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

/**
 * Created by Bruno on 10/8/2015.
 */
public class JSONToJournal extends JSONToObject<JournalEntry> {
    @Override
    public ArrayList<JournalEntry> processJSON(JSONObject object, Context c, String kpiname) throws ParseException {

        try {
            JSONArray rows = object.getJSONArray("rows");
            ArrayList<JournalEntry> entries = new ArrayList<>();
            SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");

            for (int r = 0; r < rows.length(); ++r) {
                JSONObject row = rows.getJSONObject(r);
                JSONArray columns = row.getJSONArray("c");


                String category = columns.getJSONObject(0).getString("v");
                String owner = columns.getJSONObject(1).getString("v");
                String title = columns.getJSONObject(2).getString("v");
                String desc = columns.getJSONObject(3).getString("v");
                String latest_up = columns.getJSONObject(4).getString("v");
                String last_string = columns.getJSONObject(5).getString("v");
                Date last_date = sdf.parse(last_string);





                entries.add(new JournalEntry(category,owner,title,desc,latest_up,last_date));


            }

            return entries;

        } catch (JSONException e) {
            e.printStackTrace();
        }
        return null;
    }
}
