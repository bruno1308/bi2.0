package br.edu.unifei.bi20.utils;

import android.content.Context;

import br.edu.unifei.bi20.Comments;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

/**
 * Created by Bruno on 10/6/2015.
 */
public class JSONToComments extends JSONToObject<Comments> {

    @Override
    public ArrayList<Comments> processJSON(JSONObject object, Context c,String kpiname) throws ParseException {
        try {
            JSONArray rows = object.getJSONArray("rows");
            // Category[] data = new Category[rows.length()];
            ArrayList<Comments> entries = new ArrayList<>();
            SimpleDateFormat sdf = new SimpleDateFormat("MM/yyyy");


            for (int r = 0; r < rows.length(); ++r) {
                JSONObject row = rows.getJSONObject(r);
                JSONArray columns = row.getJSONArray("c");

                for(int i=0;i<12;++i){
                    String comment = columns.getJSONObject(i+1).getString("v");
                    if(comment.equals("-")) break;
                    int mes = i;
                    Date d = sdf.parse(i+1+"/2015");


                    entries.add(new Comments(comment,d));
                }

            }

            return entries;

        } catch (JSONException e) {
            e.printStackTrace();
        }
        return null;

    }
}
