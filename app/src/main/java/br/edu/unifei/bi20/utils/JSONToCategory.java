package br.edu.unifei.bi20.utils;

import android.content.Context;
import android.widget.ListView;

import br.edu.unifei.bi20.category.Category;
import br.edu.unifei.bi20.category.CategoryAdapter;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by Bruno on 7/9/2015.
 */
public final class JSONToCategory {

    private JSONToCategory(){
        super();
    }
    public static CategoryAdapter processJson(JSONObject object, Context c) {
        //ArrayList<Category> items = new ArrayList<Category>();

        ListView listview;

        try {
            JSONArray rows = object.getJSONArray("rows");
            Category[] data = new Category[rows.length()];

            for (int r = 0; r < rows.length(); ++r) {
                JSONObject row = rows.getJSONObject(r);
                JSONArray columns = row.getJSONArray("c");

                String name = columns.getJSONObject(0).getString("v");
                Category item = new Category(name);
               // items.add(item);
                data[r] = item;
            }

            final CategoryAdapter adapter = new CategoryAdapter(c, br.edu.unifei.bi20.R.layout.listview_item, data);
            return adapter;

        } catch (JSONException e) {
            e.printStackTrace();
        }
        return null;
    }

    /*public static ArrayList<Entry> selectOne(JSONObject object) {
        ArrayList<Entry> items = new ArrayList<Entry>();
        ListView listview;

        try {
            JSONArray rows = object.getJSONArray("rows");

            for (int r = 0; r < rows.length(); ++r) {
                JSONObject row = rows.getJSONObject(r);
                JSONArray columns = row.getJSONArray("c");

                int id = columns.getJSONObject(0).getInt("v");
                double kwh = columns.getJSONObject(1).getDouble("v");
                long datal = columns.getJSONObject(2).getLong("v");
                Entry item = new Entry(id,kwh,datal);
                items.add(item);
            }

           // final SmartItemAdapter adapter = new SmartItemAdapter(c, R.layout.team, items);
            return items;

        } catch (JSONException e) {
            e.printStackTrace();
        }
        return null;
    }*/
}
