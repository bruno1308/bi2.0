package br.edu.unifei.bi20.kpi;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import br.edu.unifei.bi20.Comments;
import br.edu.unifei.bi20.SettingsActivity;
import br.edu.unifei.bi20.journal.JournalActivity;
import br.edu.unifei.bi20.utils.JSONToComments;
import br.edu.unifei.bi20.utils.JSONToKPI;
import br.edu.unifei.bi20.utils.SpreadsheetAnswer;
import br.edu.unifei.bi20.utils.SpreadsheetRequest;
import br.edu.unifei.bi20.utils.WebpageToJSON;

import org.achartengine.ChartFactory;
import org.achartengine.GraphicalView;
import org.achartengine.chart.PointStyle;
import org.achartengine.model.SeriesSelection;
import org.achartengine.model.TimeSeries;
import org.achartengine.model.XYMultipleSeriesDataset;
import org.achartengine.renderer.BasicStroke;
import org.achartengine.renderer.XYMultipleSeriesRenderer;
import org.achartengine.renderer.XYSeriesRenderer;
import org.json.JSONObject;

import java.text.Format;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

public class KPI2Activity extends AppCompatActivity {

    private GraphicalView mChart;
    private TextView name_kpi,danger,good,caution,comment;
    private String name_chosen;
    private Context c;
    ArrayList<KPIEntry> entries = null;
    ArrayList<Comments> comments = null;
    int latest_month=-1;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(br.edu.unifei.bi20.R.layout.activity_kpi2);

        Toolbar toolbar = (Toolbar) findViewById(br.edu.unifei.bi20.R.id.toolbar);
        setSupportActionBar(toolbar);
        toolbar.inflateMenu(br.edu.unifei.bi20.R.menu.menu_toolbar);
        c = this;
        danger= (TextView)findViewById(br.edu.unifei.bi20.R.id.lblDanger);
        good = (TextView)findViewById(br.edu.unifei.bi20.R.id.lblGood);
        caution = (TextView)findViewById(br.edu.unifei.bi20.R.id.lblCaution);
        comment = (TextView)findViewById(br.edu.unifei.bi20.R.id.lblComment);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
       // toolbar.setNavigationIcon(R.mipmap.janssenlogo);
        toolbar.setNavigationIcon(br.edu.unifei.bi20.R.drawable.abc_ic_ab_back_mtrl_am_alpha);
        name_chosen = getIntent().getStringExtra("kpi");
        name_kpi = (TextView) findViewById(br.edu.unifei.bi20.R.id.name_metrica);
        name_kpi.setText(name_chosen);
        name_kpi.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toast toast = Toast.makeText(c, KPI.getDescriptionfromName(name_chosen), Toast.LENGTH_LONG);
                toast.setGravity(Gravity.START | Gravity.TOP, view.getLeft(), (view.getBottom() - view.getTop()));

                RelativeLayout toastLayout;
                try {
                    toastLayout = (RelativeLayout) toast.getView();
                    TextView toastTV = (TextView) toastLayout.getChildAt(0);
                    toastTV.setTextSize(12);
                    toast.show();
                } catch (Exception e) {
                    e.printStackTrace();
                    LinearLayout toastLayout2 = (LinearLayout) toast.getView();
                    TextView toastTV = (TextView) toastLayout2.getChildAt(0);
                    toastTV.setTextSize(12);
                    toast.show();

                }


            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(br.edu.unifei.bi20.R.menu.menu_toolbar, menu);
        return true;
    }

    @Override
    protected void onStart(){
        super.onStart();
        new WebpageToJSON(new SpreadsheetAnswer() {
            @Override
            public void getJsonAnswer(JSONObject object) throws ParseException {
                try {
                    entries = new JSONToKPI().processJSON(object, c, name_chosen);

                } catch (ParseException e) {
                    e.printStackTrace();
                }finally {
                    if(entries == null || entries.size()==0){
                        Toast.makeText(c, "Couldn't access spreadsheet",Toast.LENGTH_SHORT).show();
                    }else {
                        openChart();
                    }

                }



            }
        }, this).execute(SpreadsheetRequest.selectKPIbyName((name_chosen)));
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == br.edu.unifei.bi20.R.id.action_settings) {
            Intent i = new Intent(this,SettingsActivity.class);
            startActivity(i);
            return true;
        }else if(id == br.edu.unifei.bi20.R.id.action_test){
            Intent i = new Intent(this,JournalActivity.class);
            startActivity(i);
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private void openChart() throws ParseException {
        int porcentage_flag = (!entries.get(0).kpi.isPorcentage()?1:100);


        good.setText(entries.get(0).kpi.getGood_threshold()+(porcentage_flag==100?"%":""));
        danger.setText(entries.get(0).kpi.getDanger_threshold()+(porcentage_flag==100?"%":""));
        caution.setText(entries.get(0).kpi.getGood_threshold()+(porcentage_flag==100?"%":"") + "~" +
                entries.get(0).kpi.getDanger_threshold()+(porcentage_flag==100?"%":"") );
        // Creating TimeSeries for Visits
        TimeSeries visitsSeries = new TimeSeries("Value");
        TimeSeries waterSeries = new TimeSeries("Bad");
        TimeSeries waterSeries2 = new TimeSeries("Good");



        XYSeriesRenderer waterRenderer1 = new XYSeriesRenderer();
        waterRenderer1.setStroke(BasicStroke.DASHED);
        waterRenderer1.setLineWidth(4);
        waterRenderer1.setColor(Color.RED);

        XYSeriesRenderer waterRenderer2 = new XYSeriesRenderer();
        waterRenderer2.setStroke(BasicStroke.DASHED);
        waterRenderer2.setColor(Color.GREEN);
        waterRenderer2.setLineWidth(4);

        // Creating TimeSeries for Views
        //  TimeSeries viewsSeries = new TimeSeries("Views");

        // Adding data to Visits and Views Series
        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");

        for(int i=0;i<entries.size();i++){
            if(entries.get(i).getValue()== -1) break;
            Date d = entries.get(i).getDate();
            visitsSeries.add(d, Math.round(entries.get(i).getValue()*porcentage_flag*100.0)/100.0);
            waterSeries.add(d, entries.get(0).kpi.getDanger_threshold());
            waterSeries2.add(d, entries.get(0).kpi.getGood_threshold());
            //viewsSeries.add(dt[i],views[i]);
        }


        // Creating a dataset to hold each series
        XYMultipleSeriesDataset dataset = new XYMultipleSeriesDataset();

        // Adding Series to the dataset
        dataset.addSeries(visitsSeries);
        dataset.addSeries(waterSeries);
        dataset.addSeries(waterSeries2);


        // Creating XYSeriesRenderer to customize visitsSeries
        XYSeriesRenderer visitsRenderer = new XYSeriesRenderer();
        visitsRenderer.setColor(getResources().getColor(br.edu.unifei.bi20.R.color.colorPrimaryDark));
        visitsRenderer.setChartValuesTextSize(20);
        visitsRenderer.setChartValuesSpacing(20);
        visitsRenderer.setPointStyle(PointStyle.DIAMOND);
        visitsRenderer.setPointStrokeWidth(10);
        //  visitsRenderer.setFillPoints(true);
        visitsRenderer.setLineWidth(4);
        visitsRenderer.setDisplayChartValues(true);


        // Creating a XYMultipleSeriesRenderer to customize the whole chart
        XYMultipleSeriesRenderer multiRenderer = new XYMultipleSeriesRenderer();

        multiRenderer.setChartTitle(name_chosen+" over time");
        multiRenderer.setXTitle("Months");

        multiRenderer.setYTitle("Value");
        multiRenderer.setApplyBackgroundColor(true);

        // Note: The order of adding dataseries to dataset and renderers to multipleRenderer
        // should be same
        multiRenderer.setLabelsTextSize(25);
        //multiRenderer.setLabelsColor(Color.GRAY);
        multiRenderer.setXLabelsColor(Color.GRAY);
        multiRenderer.setYLabelsColor(0, Color.GRAY);
        multiRenderer.setAxisTitleTextSize(25);
        multiRenderer.addSeriesRenderer(visitsRenderer);

        multiRenderer.addSeriesRenderer(waterRenderer1);
        multiRenderer.addSeriesRenderer(waterRenderer2);


        waterRenderer1.setDisplayChartValues(true);
        waterRenderer1.setChartValuesTextSize(10);
        waterRenderer1.setDisplayChartValues(true);
        waterRenderer2.setDisplayChartValues(true);
        waterRenderer2.setChartValuesTextSize(10);
        waterRenderer2.setDisplayChartValues(true);



        multiRenderer.setShowGrid(true);
        multiRenderer.setChartTitleTextSize(25);
        multiRenderer.setLegendTextSize(25);
        multiRenderer.setScale(1f);
        multiRenderer.setSelectableBuffer(40);
        int abc[] = new int[]{30,30,30,30};
        multiRenderer.setMargins(abc);


        // Getting a reference to LinearLayout of the MainActivity Layout
        LinearLayout chartContainer = (LinearLayout) findViewById(br.edu.unifei.bi20.R.id.chart_container);
        Log.i("LENGTH3","here1");

        // Creating a Time Chart
        mChart = (GraphicalView) ChartFactory.getTimeChartView(getBaseContext(), dataset, multiRenderer, "MMM-yyyy");

        multiRenderer.setClickEnabled(true);
        multiRenderer.setSelectableBuffer(10);
        Log.i("LENGTH3", "here2");

        //region Setting a click event listener for the graph
        mChart.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                Log.i("LENGTH3", "here6");
                Format formatter = new SimpleDateFormat("dd-MMM-yyyy");

                SeriesSelection seriesSelection = mChart.getCurrentSeriesAndPoint();

                if (seriesSelection != null) {
                    int seriesIndex = seriesSelection.getSeriesIndex();
                    String selectedSeries = "Visits";
                    if (seriesIndex == 0)
                        selectedSeries = "Value";
                    else
                        selectedSeries = "Value";

                    // Getting the clicked Date ( x value )
                    long clickedDateSeconds = (long) seriesSelection.getXValue();
                    Date clickedDate = new Date(clickedDateSeconds);
                    String strDate = formatter.format(clickedDate);

                    // Getting the y value
                    Double amount = seriesSelection.getValue();

                    // Displaying Toast Message
                    Toast.makeText(
                            getBaseContext(),
                            selectedSeries + " on " + strDate + " : " + amount,
                            Toast.LENGTH_SHORT).show();
                }
            }

        });
        //endregion Setting a click event listener for the graph

        // Adding the Line Chart to the LinearLayout
        Log.i("LENGTH3", "here3");
        chartContainer.addView(mChart);
        comment.setText(entries.get(entries.size()-1).getC().getComment());

    }

}
