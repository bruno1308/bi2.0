package br.edu.unifei.bi20.kpi;

import android.graphics.Color;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.Toast;

import br.edu.unifei.bi20.R;

import org.achartengine.ChartFactory;
import org.achartengine.GraphicalView;
import org.achartengine.chart.PointStyle;
import org.achartengine.model.SeriesSelection;
import org.achartengine.model.TimeSeries;
import org.achartengine.model.XYMultipleSeriesDataset;
import org.achartengine.renderer.BasicStroke;
import org.achartengine.renderer.XYMultipleSeriesRenderer;
import org.achartengine.renderer.XYSeriesRenderer;

import java.text.Format;
import java.text.SimpleDateFormat;
import java.util.Date;

public class KPIActivity extends AppCompatActivity {
    private GraphicalView mChart;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_kpi);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        toolbar.inflateMenu(R.menu.menu_toolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        toolbar.setNavigationIcon(R.drawable.home);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_toolbar, menu);
        return true;
    }
    @Override
    protected void onStart() {
        super.onStart();
        openChart();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }


        return super.onOptionsItemSelected(item);
    }

    private void openChart(){


        double[] visits = {98.8,95.0, 96.0, 93.1, 95.0, 100};
        long[] years = {
                1104538000,  // 2001
                1107138000, // 2002
                1109738000, // 2003
                1112338000, // 2004
                1114938000,  // 2005
                1117538000  // 2005
        };



        // Creating TimeSeries for Visits
        TimeSeries visitsSeries = new TimeSeries("Value");
        TimeSeries waterSeries = new TimeSeries("Bad");



        TimeSeries waterSeries2 = new TimeSeries("Good");



        XYSeriesRenderer waterRenderer1 = new XYSeriesRenderer();
        waterRenderer1.setStroke(BasicStroke.DASHED);
        waterRenderer1.setLineWidth(4);
        waterRenderer1.setColor(Color.RED);

        XYSeriesRenderer waterRenderer2 = new XYSeriesRenderer();
        waterRenderer2.setStroke(BasicStroke.DASHED);
        waterRenderer2.setColor(Color.GREEN);
        waterRenderer2.setLineWidth(4);

        // Creating TimeSeries for Views
        //  TimeSeries viewsSeries = new TimeSeries("Views");

        // Adding data to Visits and Views Series

        for(int i=0;i<years.length;i++){

            visitsSeries.add(new Date(years[i]*1000), visits[i]);
            waterSeries.add(new Date(years[i]*1000), 93.5);
            waterSeries2.add(new Date(years[i]*1000), 98.5);
            //viewsSeries.add(dt[i],views[i]);
        }
        Log.i("LENGTH3", "here");

        // Creating a dataset to hold each series
        XYMultipleSeriesDataset dataset = new XYMultipleSeriesDataset();

        // Adding Visits Series to the dataset
        dataset.addSeries(visitsSeries);
        dataset.addSeries(waterSeries);
        dataset.addSeries(waterSeries2);

        // Adding Visits Series to dataset
        // dataset.addSeries(viewsSeries);

        // Creating XYSeriesRenderer to customize visitsSeries
        XYSeriesRenderer visitsRenderer = new XYSeriesRenderer();
        visitsRenderer.setColor(getResources().getColor(R.color.colorPrimaryDark));
        visitsRenderer.setChartValuesTextSize(20);
        visitsRenderer.setChartValuesSpacing(20);
        visitsRenderer.setPointStyle(PointStyle.DIAMOND);
        visitsRenderer.setPointStrokeWidth(10);
        //  visitsRenderer.setFillPoints(true);
        visitsRenderer.setLineWidth(4);
        visitsRenderer.setDisplayChartValues(true);

        // Creating XYSeriesRenderer to customize viewsSeries
       /* XYSeriesRenderer viewsRenderer = new XYSeriesRenderer();
        viewsRenderer.setColor(Color.YELLOW);
        viewsRenderer.setPointStyle(PointStyle.DIAMOND);
        viewsRenderer.setPointStrokeWidth(10);
       // viewsRenderer.setFillPoints(true);
        viewsRenderer.setDisplayBoundingPoints(true);
        viewsRenderer.setLineWidth(4);
        viewsRenderer.setDisplayChartValues(true);*/

        // Creating a XYMultipleSeriesRenderer to customize the whole chart
        XYMultipleSeriesRenderer multiRenderer = new XYMultipleSeriesRenderer();

        multiRenderer.setChartTitle("Days of Supply over time");
        multiRenderer.setXTitle("Months");

        multiRenderer.setYTitle("Value");
        multiRenderer.setApplyBackgroundColor(true);
        //  int bg = (int)Long.parseLong("212121", 16);
        // multiRenderer.setMarginsColor(Color.argb(0x00, 0x01, 0x01, 0x01));
        //multiRenderer.setBackgroundColor(bg);

        // Adding visitsRenderer and viewsRenderer to multipleRenderer
        // Note: The order of adding dataseries to dataset and renderers to multipleRenderer
        // should be same
        multiRenderer.setLabelsTextSize(25);
        //multiRenderer.setLabelsColor(Color.GRAY);
        multiRenderer.setXLabelsColor(Color.GRAY);
        multiRenderer.setYLabelsColor(0, Color.GRAY);
        multiRenderer.setAxisTitleTextSize(25);
        multiRenderer.addSeriesRenderer(visitsRenderer);

        multiRenderer.addSeriesRenderer(waterRenderer1);
        multiRenderer.addSeriesRenderer(waterRenderer2);


        waterRenderer1.setDisplayChartValues(true);
        waterRenderer1.setChartValuesTextSize(10);
        waterRenderer1.setDisplayChartValues(true);
        waterRenderer2.setDisplayChartValues(true);
        waterRenderer2.setChartValuesTextSize(10);
        waterRenderer2.setDisplayChartValues(true);



        multiRenderer.setShowGrid(true);
        multiRenderer.setChartTitleTextSize(25);
        multiRenderer.setLegendTextSize(25);
        multiRenderer.setScale(1f);
        int abc[] = new int[]{30,30,30,30};
        multiRenderer.setMargins(abc);
        //multiRenderer.setLegendHeight(1);

        // multiRenderer.addSeriesRenderer(viewsRenderer);

        // Getting a reference to LinearLayout of the MainActivity Layout
        LinearLayout chartContainer = (LinearLayout) findViewById(R.id.chart_container);
        Log.i("LENGTH3","here1");

        // Creating a Time Chart
        mChart = (GraphicalView) ChartFactory.getTimeChartView(getBaseContext(), dataset, multiRenderer, "MMM-yyyy");

        multiRenderer.setClickEnabled(true);
        multiRenderer.setSelectableBuffer(10);
        Log.i("LENGTH3", "here2");

        // Setting a click event listener for the graph
        mChart.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                Log.i("LENGTH3", "here6");
                Format formatter = new SimpleDateFormat("dd-MMM-yyyy");

                SeriesSelection seriesSelection = mChart.getCurrentSeriesAndPoint();

                if (seriesSelection != null) {
                    int seriesIndex = seriesSelection.getSeriesIndex();
                    String selectedSeries = "Visits";
                    if (seriesIndex == 0)
                        selectedSeries = "Value";
                    else
                        selectedSeries = "Views";

                    // Getting the clicked Date ( x value )
                    long clickedDateSeconds = (long) seriesSelection.getXValue();
                    Date clickedDate = new Date(clickedDateSeconds);
                    String strDate = formatter.format(clickedDate);

                    // Getting the y value
                    int amount = (int) seriesSelection.getValue();

                    // Displaying Toast Message
                    Toast.makeText(
                            getBaseContext(),
                            selectedSeries + " on " + strDate + " : " + amount,
                            Toast.LENGTH_SHORT).show();
                }
            }

        });

        // Adding the Line Chart to the LinearLayout
        Log.i("LENGTH3", "here3");
        chartContainer.addView(mChart);

    }
}
