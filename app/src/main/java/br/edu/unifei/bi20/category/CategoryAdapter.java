package br.edu.unifei.bi20.category;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.CheckedTextView;

import br.edu.unifei.bi20.R;

/**
 * Created by Bruno on 08/09/2015.
 */
public class CategoryAdapter extends ArrayAdapter<Category> {

    Context context;
    int layoutResourceId;
    Category data[] = null;
    public CategoryAdapter(Context context, int resource, Category[] objects) {
        super(context, resource, objects);
        this.data = objects;
        layoutResourceId = resource;
        this.context = context;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View row = convertView;
        CategoryHolder holder = null;

        if (row == null)
        {
            LayoutInflater inflater = ((Activity)context).getLayoutInflater();
            row = inflater.inflate(R.layout.listview_category, parent, false);

            holder = new CategoryHolder();
            holder.txtName = (CheckedTextView)row.findViewById(R.id.chkText1);
            row.setTag(holder);
        }
        else {
            holder = (CategoryHolder)row.getTag();
        }

            Category m = data[position];
        if(m!= null) {
            holder.txtName.setText(m.getNome());
            holder.txtName.setChecked(m.isFavorite());
        }


        return row;
    }

    static class CategoryHolder
    {
        public CheckedTextView txtName;
    }
    public Category[] getItems(){
        return data;
    }



}
