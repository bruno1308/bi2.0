package br.edu.unifei.bi20.utils;

/**
 * Created by Bruno on 07/07/2015.
 */

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.Toast;

import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.ParseException;

public class WebpageToJSON extends AsyncTask<String, Void, String> {
    SpreadsheetAnswer answer;
    private ProgressDialog dialog;
    private Context context;
    public WebpageToJSON(SpreadsheetAnswer answerp, Context c) {
        this.answer = answerp;
        context = c;
        dialog  = new ProgressDialog(context);
    }

    protected void onPreExecute() {
        this.dialog.setMessage("Downloading info...");
        this.dialog.show();
    }
    @Override
    protected String doInBackground(String... urls) {
        // params comes from the execute() call: params[0] is the url.
        try {
            Log.i("DOWNLOAD","RETURNING DOWNLOADURL, MY url IS: "+urls[0]);

            return downloadUrl(urls[0]);
        } catch (IOException e) {
            Log.i("DOWNLOAD","RETURNING ERROR");
            e.printStackTrace();
            return "Error";
        }
    }
    // onPostExecute displays the results of the AsyncTask.
    @Override
    protected void onPostExecute(String result) {
        // remove the unnecessary parts from the response and construct a JSON
        Log.i("DOWNLOAD","COMPLETING");
        dialog.dismiss();
        if(result.equals("Error")){
            Toast.makeText(context, "Error, please check your internet connection", Toast.LENGTH_LONG).show();
        }else {
            int start = result.indexOf("{", result.indexOf("{") + 1);
            int end = result.lastIndexOf("}");
            int datebegin = result.indexOf("{\"id\":\"B\",\"label\":");
            int dateend = result.indexOf("\"", datebegin + 1);
            String date;
            if(datebegin!= -1) date = result.substring(datebegin,dateend);
            String jsonResponse = result.substring(start, end);
            try {
                JSONObject table = new JSONObject(jsonResponse);
                answer.getJsonAnswer(table);
                Log.i("DOWNLOAD","COMPLETE");
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
    private String downloadUrl(String urlString) throws IOException {
        Log.i("DOWNLOAD","STARTED");
        InputStream is = null;
        try {
            URL url = new URL(urlString);
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setReadTimeout(10000 /* milliseconds */);
            conn.setConnectTimeout(15000 /* milliseconds */);
            conn.setRequestMethod("GET");
            conn.setDoInput(true);
            // Starts the query
            conn.connect();
            int responseCode = conn.getResponseCode();
            is = conn.getInputStream();
            String contentAsString = convertStreamToString(is);
            Log.i("DOWNLOAD","RETURNED");
            return contentAsString;
        } finally {
            if (is != null) {
                Log.i("DOWNLOAD","IS != NULL");
                is.close();
            }
        }
    }
    private String convertStreamToString(InputStream is) {
        Log.i("DOWNLOAD","CONVERTING TO STRING");
        BufferedReader reader = new BufferedReader(new InputStreamReader(is));
        StringBuilder sb = new StringBuilder();
        String line = null;
        try {
            while ((line = reader.readLine()) != null) {
                sb.append(line + "\n");
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                is.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        Log.i("DOWNLOAD","DONE CONVERTING");
        return sb.toString();
    }
}