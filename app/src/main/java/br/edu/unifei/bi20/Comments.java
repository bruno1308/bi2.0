package br.edu.unifei.bi20;

import java.util.Date;

/**
 * Created by Bruno on 10/6/2015.
 */
public class Comments {
   private  String comment;

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public Date getData() {
        return data;
    }

    public void setData(Date data) {
        this.data = data;
    }

    private Date data;

    public Comments(String n, Date d){
        comment = n;
        data = d;
    }
}
