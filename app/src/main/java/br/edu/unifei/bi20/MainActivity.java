package br.edu.unifei.bi20;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.GridView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;


import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.json.JSONObject;

import br.edu.unifei.bi20.journal.JournalActivity;
import br.edu.unifei.bi20.kpi.KPI;
import br.edu.unifei.bi20.kpi.KPIEntry;
import br.edu.unifei.bi20.utils.DatabaseHelper;
import br.edu.unifei.bi20.kpi.KPI2Activity;
import br.edu.unifei.bi20.kpi.KPIAdapter;
import br.edu.unifei.bi20.utils.JSONToKPI;
import br.edu.unifei.bi20.utils.SpreadsheetAnswer;
import br.edu.unifei.bi20.utils.SpreadsheetRequest;
import br.edu.unifei.bi20.utils.WebpageToJSON;

import java.lang.reflect.Type;
import java.text.ParseException;
import java.util.ArrayList;


public class MainActivity extends AppCompatActivity {

    private ListView listView1;
    private GridView gridView1;
    private TextView lblmonth;
    SharedPreferences sp;
    ArrayList<KPIEntry> favorites;
    ArrayList<KPIEntry> default_data;
    ArrayList<KPIEntry> entries = null;
    KPIAdapter myadapter;
    Context c;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        DatabaseHelper db = new DatabaseHelper(this);
        sp = this.getSharedPreferences(DatabaseHelper.PREFS_NAME,
                Context.MODE_PRIVATE);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        toolbar.inflateMenu(R.menu.menu_toolbar);
        c = this;

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        toolbar.setNavigationIcon(R.mipmap.homewhite);

        favorites = new ArrayList<>();
        default_data = new ArrayList<>();


      //  listView1 = (ListView)findViewById(R.id.listView1); Para lista, apague esses dois comentarios
      //  listView1.setAdapter(myadapter);

        gridView1 = (GridView)findViewById(R.id.gridView1);
        //gridView1.setAdapter(myadapter);

        gridView1.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position,
                                    long id) {
                TextView kpiclicked = (TextView)view.findViewById(R.id.txtTitle);
                Intent intent = new Intent(MainActivity.this, KPI2Activity.class);
                intent.putExtra("kpi",kpiclicked.getText());
                startActivity(intent);
            }
        });

        View myLayout = findViewById( R.id.footer ); // root View id from that link
        Button btnFavorites = (Button) myLayout.findViewById( R.id.btnFavorites); // id of a view contained in the included file
        Button btnDefault = (Button) myLayout.findViewById( R.id.btnDefault);

        btnFavorites.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onClickFavorite(v);

            }
        });

        btnDefault.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onClickDefault(v);

            }
        });

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_toolbar, menu);
        return true;
    }

    @Override
    protected void onStart(){
        super.onStart();
        downloadOrLoad('d');

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            Intent i = new Intent(this,SettingsActivity.class);
            startActivity(i);
            return true;
        }else if(id == R.id.action_test){
            Intent i = new Intent(this,JournalActivity.class);
            startActivity(i);
            return true;
        }


        return super.onOptionsItemSelected(item);
    }

    public void onClickFavorite(View v) {

        downloadOrLoad('f');

       // metrica_data.clear();
       // metrica_data.addAll(favorites);

      //  myadapter.notifyDataSetChanged();
        //gridView1.setAdapter(myadapter);


    }

    public void onClickDefault(View v){
        downloadOrLoad('d');


    }

    public void updateInfos(){

        new WebpageToJSON(new SpreadsheetAnswer() {
            @Override
            public void getJsonAnswer(JSONObject object) {
                try {
                    JSONToKPI j = new JSONToKPI();
                    entries = j.processJSON(object,null,"Back Order");
                } catch (ParseException e) {
                    e.printStackTrace();
                }finally {
                    if(entries == null || entries.size()==0){
                        Toast.makeText(c, "Couldn't access spreadsheet", Toast.LENGTH_SHORT).show();
                    }else {
                        SharedPreferences.Editor prefsEditor = sp.edit();
                        Gson gson = new Gson();
                        Type listOfTestObject = new TypeToken<ArrayList<KPIEntry>>(){}.getType();
                        String s = gson.toJson(entries, listOfTestObject);
                        prefsEditor.putString(DatabaseHelper.KPI_ENTRIES, s);
                        prefsEditor.commit();
                        displayInfos('d');
                       // List<TestObject> list2 = gson.fromJson(s, listOfTestObject);
                    }

                }



            }
        }, this).execute(SpreadsheetRequest.selectKPIbyName("Back Order"));

    }
    private void displayInfos(char opt){
        ArrayList<KPIEntry> display = new ArrayList<>();
        favorites.clear();
        for(KPIEntry k:entries){
            k.kpi.setDescription(KPI.getDescriptionfromName(k.kpi.getName_kpi()));
            boolean b = sp.getBoolean(k.kpi.getName_kpi(),true);
            if(b){
                favorites.add(k);
            }
        }
        if(opt=='f'){
            display.addAll(favorites);
        }else{
            display.addAll(entries);
        }
        if(myadapter!= null){
            myadapter.clear();
            myadapter.addAll(display);
            myadapter.notifyDataSetChanged();

        }else{
            myadapter = new KPIAdapter(this,R.layout.gridview_item,display);
            gridView1.setAdapter(myadapter);
        }





    }

    private void downloadOrLoad(char c){
        entries = new ArrayList<>();
        String kpientries = sp.getString(DatabaseHelper.KPI_ENTRIES, "");
        if(kpientries == ""){
            updateInfos();
        }else{
            Gson gson = new Gson();
            Type listOfTestObject = new TypeToken<ArrayList<KPIEntry>>(){}.getType();
            entries = gson.fromJson(kpientries,listOfTestObject);
            if(entries != null){
                displayInfos(c);
            }

        }
    }
}
