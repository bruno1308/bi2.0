package br.edu.unifei.bi20.utils;

import org.json.JSONObject;

import java.text.ParseException;

/**
 * Created by Bruno on 08/07/2015.
 */
public interface SpreadsheetAnswer {
    public void getJsonAnswer(JSONObject jsonAnswer) throws ParseException;
}
