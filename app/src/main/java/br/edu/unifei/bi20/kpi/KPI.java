package br.edu.unifei.bi20.kpi;


import br.edu.unifei.bi20.R;
/**
 * Created by Bruno on 9/1/2015.
 */
public class KPI {
    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    private int status;
    private int icon;
    private String name_kpi;
    private Double good_threshold;
    private Double danger_threshold;

    public boolean isPorcentage() {
        return isPorcentage;
    }

    public void setIsPorcentage(boolean isPorcentage) {
        this.isPorcentage = isPorcentage;
    }

    private boolean isPorcentage = false;

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    private String description;
    private boolean isFavorite;

    public char getGood_char() {
        return good_char;
    }

    public void setGood_char(char good_char) {
        this.good_char = good_char;
    }

    private char good_char;

    public char getBad_char() {
        return bad_char;
    }

    public void setBad_char(char bad_char) {
        this.bad_char = bad_char;
    }

    private char bad_char;
    public KPI(int i, String n, int s){
        status = s;
        setIcon(i);
        setName_kpi(n);
    }
    public KPI(){

    }


    public static int getIconfromName(String name) {
        switch (name) {
            case "Last Work Day Case":
                return R.mipmap.lwdc;
            case "Line Item Fill Rate":
                return R.mipmap.lifr;
            case "Back Order":
                return R.mipmap.backorder;
            case "Deviation with Extension":
                return R.mipmap.devextension;
            case "Deviation without Extension":
                return R.mipmap.devextension;
            case "Schedule Attainment":
                return R.mipmap.scheduleatt;
            case "On Time Release":
                return R.mipmap.scheduleatt;
            case "Deviation Free Right First Time":
                return R.mipmap.dfrft;
            case "Volume":
                return R.mipmap.volume;
            case "Days of Supply":
                return R.mipmap.volume;
            case "Variable Cost":
                return R.mipmap.cost;
            case "Total Cost":
                return R.mipmap.cost;
            default:
                return R.mipmap.newkpi;

        }
    }

    public static int getStatusfromName(String name){
        switch (name) {
            case "Last Work Day Case": return R.mipmap.greendot;
            case "Line Item Fill Rate": return R.mipmap.greendot;
            case "Back Order": return R.mipmap.yellowdot;
            case "Deviation with Extension": return R.mipmap.greendot;
            case "Deviation without Extension": return R.mipmap.reddot;
            case "Schedule Attainment": return R.mipmap.greendot;
            case "On Time Release": return R.mipmap.greendot;
            case "Deviation Free Right First Time": return R.mipmap.greendot;
            case "Volume": return R.mipmap.yellowdot;
            case "Days of Supply": return R.mipmap.greendot;
            case "Variable Cost": return R.mipmap.yellowdot;
            case "Total Cost": return R.mipmap.reddot;
            default: return R.mipmap.greendot;

        }
        /*new KPI(R.mipmap.lwdc, "Last Work Day Case",R.mipmap.greendot),
                        new KPI(R.mipmap.lifr, "LIFR",R.mipmap.greendot),
                        new KPI(R.mipmap.backorder, "Back Order",R.mipmap.reddot),
                        new KPI(R.mipmap.devextension, "Deviation with Extension",R.mipmap.greendot),
                        new KPI(R.mipmap.devextension, "Deviation without Extension",R.mipmap.yellowdot),
                        new KPI(R.mipmap.scheduleatt, "Schedule Attainment",R.mipmap.greendot),
                        new KPI(R.mipmap.scheduleatt, "On Time Release",R.mipmap.greendot),
                        new KPI(R.mipmap.dfrft, "Deviation Free Right First Time",R.mipmap.greendot),
                        new KPI(R.mipmap.volume, "Volume",R.mipmap.greendot),
                        new KPI(R.mipmap.volume, "Days of Supply",R.mipmap.greendot),
                        new KPI(R.mipmap.cost, "Variable Cost",R.mipmap.yellowdot),
                        new KPI(R.mipmap.cost, "Total Cost",R.mipmap.reddot)
                        */
    }

    public static String getDescriptionfromName(String name){
        switch (name) {
            case "Last Work Day Case":
                return "An injury where an employee has been absent for at\n" +
                        "least one full day, in addition to the day on which the \n" +
                        "injury occurred.";
            case "Line Item Fill Rate":
            case"LIFR":
                return "Measurement how often the affiliate fulfills total\n" +
                        "quantities requested of each line item ordered by your\n" +
                        "end customer.";
            case "Back Order":
                return "Is a measure of orders that can not be attended \n" +
                        "according to expected date.";
            case "Deviation with Extension":
            case "Deviation without Extension":
                return "Ability to complete testing and review within the \n" +
                        "prescribed batch release time and quantity.";
            case "Schedule Attainment":
                return "Ability to achieve scheduled production for a set\n" +
                        "window of time.";
            case "On Time Release":
                return "Ability to complete testing and review within the \n" +
                        "prescribed batch release time and quantity.";
            case "Deviation Free Right First Time":
                return "Ability to produce batches that do not require rework,\n" +
                        "reprocessing or that is not rejected.  All process steps.";
            case "Volume":
                return "Represent the  volume of production.\n" +
                        "\n" +
                        "Volume x 1.000";
            case "Days of Supply":
                return "How long stocks and receipts will cover your \n" +
                        "requirements, to avoid product shortages or stock \n" +
                        "levels that are too high.";
            case "Variable Cost":
                return "Variable costs are those costs that vary depending on\n" +
                        "a company's production volume";
            case "Total Cost":
                return "Total Plant Spend  (Variable Cost + Fixed Cost).";
            default:
                return "No description yet.";

        }

    }

    public boolean isFavorite() {
        return isFavorite;
    }

    public void setIsFavorite(boolean isFavorite) {
        this.isFavorite = isFavorite;
    }

    public int getIcon() {
        return icon;
    }

    public void setIcon(int icon) {
        this.icon = icon;
    }

    public String getName_kpi() {
        return name_kpi;
    }

    public void setName_kpi(String name_kpi) {
        this.name_kpi = name_kpi;
    }

    public Double getGood_threshold() {
        return good_threshold;
    }

    public void setGood_threshold(Double good_threshold) {
        this.good_threshold = good_threshold;
    }

    public Double getDanger_threshold() {
        return danger_threshold;
    }

    public void setDanger_threshold(Double danger_threshold) {
        this.danger_threshold = danger_threshold;
    }
}
