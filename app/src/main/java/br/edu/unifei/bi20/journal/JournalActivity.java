package br.edu.unifei.bi20.journal;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ExpandableListView;
import android.widget.Toast;

import br.edu.unifei.bi20.R;


import br.edu.unifei.bi20.SettingsActivity;
import br.edu.unifei.bi20.utils.JSONToJournal;
import br.edu.unifei.bi20.utils.SpreadsheetAnswer;
import br.edu.unifei.bi20.utils.SpreadsheetRequest;
import br.edu.unifei.bi20.utils.WebpageToJSON;

import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class JournalActivity extends AppCompatActivity {
    JournalAdapter listAdapter;
    ExpandableListView expListView;
    List<String> listDataHeader;
    HashMap<String, List<String>> listDataChild;
    ArrayList<JournalEntry> entries = null;
    Context c;
    SharedPreferences sp;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_journal);

        // get the listview
        expListView = (ExpandableListView) findViewById(R.id.lvExp);

        // preparing list data



        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        toolbar.inflateMenu(R.menu.menu_toolbar);
        c= this;
        sp = PreferenceManager.getDefaultSharedPreferences(this);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
      //  toolbar.setNavigationIcon(R.mipmap.janssenlogo);
        toolbar.setNavigationIcon(R.drawable.abc_ic_ab_back_mtrl_am_alpha);


    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_toolbar, menu);
        return true;
    }

    @Override
    public void onStart(){
        super.onStart();
        prepareListData();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            Intent i = new Intent(this,SettingsActivity.class);
            startActivity(i);
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
    private void prepareListData() {
        listDataHeader = new ArrayList<String>();
        listDataChild = new HashMap<String, List<String>>();

        new WebpageToJSON(new SpreadsheetAnswer() {
            @Override
            public void getJsonAnswer(JSONObject object) throws ParseException {
               entries = new JSONToJournal().processJSON(object,c,"");

                if(entries == null){
                    Toast.makeText(c, "Couldn't access spreadsheet", Toast.LENGTH_SHORT).show();
                }else {
                    SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
                    for(JournalEntry j: entries){
                        boolean b = sp.getBoolean(j.getCategory(),false);
                        if(!b) continue;

                        String complete_title = j.getCategory()+ ": "+j.getTitle() + " by "+j.getOwner();
                        String description = j.getDesc()+"\n\n"+j.getLatest()+" \nUpdated on "+sdf.format(j.getLast_date());
                        List<String> mylist = new ArrayList<String>();
                        mylist.add(description);

                        listDataHeader.add(complete_title);
                        listDataChild.put(complete_title, mylist);
                    }
                    listAdapter = new JournalAdapter(c, listDataHeader, listDataChild);

                    // setting list adapter
                    expListView.setAdapter(listAdapter);

                }


            }
        }, this).execute(SpreadsheetRequest.selectAllJournalEntries());

        // Adding child data
       /* listDataHeader.add("Tylenol problem");
        listDataHeader.add("Lack of Band-aids");
        listDataHeader.add("Small diaper");

        // Adding child data
        List<String> top250 = new ArrayList<>();
        top250.add("There was a problem with Tylenol etc etc etc etc. \nFor more info, please visit jnj.com/a1b2c3");


        List<String> nowShowing = new ArrayList<>();
        nowShowing.add("Band aids are not working properly, we have to fix it!!  \n" +
                "For more info, please visit jnj.com/d4e5f6");


        List<String> comingSoon = new ArrayList<>();
        comingSoon.add("Diapers are not fitting people. \n" +
                "For more info, please visit jnj.com/h8i9j10");


        listDataChild.put(listDataHeader.get(0), top250); // Header, Child data
        listDataChild.put(listDataHeader.get(1), nowShowing);
        listDataChild.put(listDataHeader.get(2), comingSoon);*/
    }
}
