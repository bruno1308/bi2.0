package br.edu.unifei.bi20;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.preference.ListPreference;
import android.preference.Preference;
import android.preference.PreferenceFragment;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.EditText;

import br.edu.unifei.bi20.category.CategoryActivity;

public class SettingsActivity extends AppCompatActivity {

    static EditText display;
    static SharedPreferences spref;
    static Context c;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(br.edu.unifei.bi20.R.layout.activity_settings);
        //display = (EditText) findViewById(R.id.txtDisplay);
        Toolbar toolbar = (Toolbar) findViewById(br.edu.unifei.bi20.R.id.toolbar_settings);
        setSupportActionBar(toolbar);
        spref = PreferenceManager.getDefaultSharedPreferences(this);
        c = this.getApplicationContext();
       // toolbar.inflateMenu(R.menu.menu_toolbar);


        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        getFragmentManager().beginTransaction().replace(R.id.content_frame, new MyPreferenceFragment()).commit();


    }

    public static class MyPreferenceFragment extends PreferenceFragment {
        @Override
        public void onCreate(final Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            addPreferencesFromResource(br.edu.unifei.bi20.R.xml.prefs);

            final ListPreference checkboxPref = (ListPreference) getPreferenceManager().findPreference("notifications");

            checkboxPref.setOnPreferenceChangeListener(new Preference.OnPreferenceChangeListener() {
                public boolean onPreferenceChange(Preference preference, Object newValue) {
                    Log.d("MyApp", "Pref " + preference.getKey() + " changed to " + newValue.toString());
                    displaySharedPreferences();
                    return true;
                }
            });

            final Preference buttonPref = (Preference) getPreferenceManager().findPreference("openFavorites");
            buttonPref.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {

                @Override
                public boolean onPreferenceClick(Preference preference) {
                    Intent i = new Intent(c, FavoritesActivity.class);
                    startActivity(i);
                    return false;
                }
            });

            final Preference buttonCat = (Preference) getPreferenceManager().findPreference("openCategories");
            buttonCat.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {

                @Override
                public boolean onPreferenceClick(Preference preference) {
                    Intent i = new Intent(c,CategoryActivity.class);
                    startActivity(i);
                    return false;
                }
            });

            final Preference buttonAbout = (Preference) getPreferenceManager().findPreference("openAbout");
            buttonAbout.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {

                @Override
                public boolean onPreferenceClick(Preference preference) {
                    Intent i = new Intent(c,AboutActivity.class);
                    startActivity(i);
                    return false;
                }
            });

        }
    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);

       /* LinearLayout root = (LinearLayout)findViewById(android.R.id.list).getParent().getParent().getParent();
        Toolbar bar = (Toolbar) LayoutInflater.from(this).inflate(R.layout.header_settings, root, false);
        root.addView(bar, 0); // insert at top
        bar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });*/
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(br.edu.unifei.bi20.R.menu.menu_settings, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }


        return super.onOptionsItemSelected(item);
    }

     public static void displaySharedPreferences() {




    }
}
