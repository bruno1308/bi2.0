package br.edu.unifei.bi20.utils;

import br.edu.unifei.bi20.category.Category;

/**
 * Created by Bruno on 7/9/2015.
 */

public final class SpreadsheetRequest {
    //public static String KEY = "1bBxP86CBD_tp7lJ0E6C9NDI4PIcNTqxZVL85mnlFtTk";
    public static String KEY = "1Oq-4FaapaIOMyLXMAKHqGKKtUSZ75132xLW4438_PQc";
    public static String BASE = "https://docs.google.com/spreadsheets/d/key/gviz/tq?sheet=name&headers=1&tq=query";
    private static String SELECTALL = "https://spreadsheets.google.com/tq?key=1Oq-4FaapaIOMyLXMAKHqGKKtUSZ75132xLW4438_PQc";
    private static String SELECTALLCAT = "https://docs.google.com/spreadsheets/d/key/gviz/tq?sheet=name&headers=1";
    //private static String SELECTTRANS = "https://docs.google.com/a/google.com/spreadsheets/d/1cenwTRlrGMzMTufkK27IIgAHvEGwVKR9Yr59Qako3u0/gviz/tq?tq=select%20*%20where%20B%20contains%20'TRANSFORMADOR'";
    private static String CATEGORIES ="category";
    private static String KPI2015 = "kpi2015";
    private static String KPI2015COMMENTS = "kpi2015comments";
    private static String JOURNAL = "stem";
    private static final String KPI_NAMES[] = {"BackOrder","DaysOfSupply","DevWithExt","DevWithoutExt","DFRFT","LWDC","LIFR","OTR","SA","TCost","VCost","Vol"};
  //  private static String MEASURESDB ="Sheet2";

    public static String selectKPIbyName(String name){
        name = kpiNametoSheetName(name);
        String request= SELECTALLCAT;
        request = request.replace("key", KEY);
        request = request.replace("name", name);
        return request;
    }

    public static String selectType(Category t){
        String query="";
        String request= BASE;
        request = query.replace("key",KEY);
        request = query.replace("name",CATEGORIES);
        query = "select * where A contains ";
        query = query.concat("'"+t.getNome()+"'");
        query = EncodingUtil.encodeURIComponent(query);
        request = request.replace("query",query);
        return request;
    }
    public static String selectCommentByKPI(String name){
        String query="";
        String request= BASE;
        request = request.replace("key",KEY);
        request = request.replace("name",KPI2015COMMENTS);
        query = "select * where A contains ";
        query = query.concat("'"+name+"'");
        query = EncodingUtil.encodeURIComponent(query);
        request = request.replace("query",query);
        return request;
    }

    public static String selectAll(){
        return SELECTALL;
    }
    public static String selectAllCategories(){
        String request= SELECTALLCAT;
        request = request.replace("key", KEY);
        request = request.replace("name",CATEGORIES);
        return request;
    }
    public static String selectAllKPI(){
        String request= SELECTALLCAT;
        request = request.replace("key", KEY);
        request = request.replace("name", KPI2015);
        return request;
    }

    public static String selectAllJournalEntries(){
        String request= SELECTALLCAT;
        request = request.replace("key", KEY);
        request = request.replace("name", JOURNAL);
        return request;
    }


    /*public static String selectItem(int id){
        String query="";
        String request= BASE;
        request = request.replace("key",KEY);
        request = request.replace("name",MEASURESDB);
        query = "select * where A = "+id;
        query = EncodingUtil.encodeURIComponent(query);
        request = request.replace("query",query);
        return request;
    }*/

    public static String kpiNametoSheetName(String n){
        switch (n) {
            case "Back Order":
                return KPI_NAMES[0];
            case "Days of Supply":
                return KPI_NAMES[1];
            case "Deviation with Extension":
                return KPI_NAMES[2];
            case "Deviation without Extension":
                return KPI_NAMES[3];
            case "DFRFT":
                return KPI_NAMES[4];
            case "Last Work Day Case":
                return KPI_NAMES[5];
            case "Line Item Fill Rate":
                return KPI_NAMES[6];
            case "On Time Release":
                return KPI_NAMES[7];
            case "Schedule Attainment":
                return KPI_NAMES[8];
            case "Total Cost":
                return KPI_NAMES[9];
            case "Variable Cost":
                return KPI_NAMES[10];
            case "Volume":
                return KPI_NAMES[11];
            default:
                return "-1";
        }
    }

}
