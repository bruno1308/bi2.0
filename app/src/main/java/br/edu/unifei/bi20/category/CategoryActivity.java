package br.edu.unifei.bi20.category;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.CheckedTextView;
import android.widget.ListView;
import android.widget.Toast;

import br.edu.unifei.bi20.R;

import br.edu.unifei.bi20.utils.JSONToCategory;
import br.edu.unifei.bi20.utils.SpreadsheetAnswer;
import br.edu.unifei.bi20.utils.SpreadsheetRequest;
import br.edu.unifei.bi20.utils.WebpageToJSON;

import org.json.JSONObject;

public class CategoryActivity extends AppCompatActivity {
    ListView listview;
    SharedPreferences sp;
    Context c;
    Category [] mycategories = null;
    CategoryAdapter myadapter;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_category);

        listview = (ListView) findViewById(R.id.lstCategories);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar_categories);
        setSupportActionBar(toolbar);
        toolbar.inflateMenu(R.menu.menu_category);
        c = this;

        sp = PreferenceManager.getDefaultSharedPreferences(this);


        listview.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> arg0, View arg1, int arg2,
                                    long arg3) {
                CheckedTextView checkedTextView = (CheckedTextView) arg1.findViewById(R.id.chkText1);
                checkedTextView.toggle();
                //String key = metrica_data[arg2].name_kpi;
                mycategories[arg2].setIsFavorite(checkedTextView.isChecked());
                SharedPreferences.Editor editor = sp.edit();

                editor.putBoolean(mycategories[arg2].getNome(),mycategories[arg2].isFavorite());
                editor.commit();
                myadapter.notifyDataSetChanged();
                //listView1.setItemChecked(arg2, false);

            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_category, menu);
        return true;
    }

    @Override
    protected void onStart(){
        super.onStart();
        new WebpageToJSON(new SpreadsheetAnswer() {
            @Override
            public void getJsonAnswer(JSONObject object) {
                listview.setAdapter(JSONToCategory.processJson(object, c));
                myadapter = (CategoryAdapter)listview.getAdapter();
                if(myadapter == null){
                    Toast.makeText(c, "Couldn't access spreadsheet",Toast.LENGTH_SHORT).show();
                }else {
                    mycategories = myadapter.getItems();
                    initializeChecks();
                }


            }
        }, this).execute(SpreadsheetRequest.selectAllCategories());
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private void initializeChecks(){
        for(Category k:mycategories){
            boolean b = sp.getBoolean(k.getNome(),false);
            k.setIsFavorite(b);
        }
    }
}
