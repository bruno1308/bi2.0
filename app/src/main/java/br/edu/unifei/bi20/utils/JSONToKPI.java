package br.edu.unifei.bi20.utils;

import android.content.Context;

import br.edu.unifei.bi20.Comments;
import br.edu.unifei.bi20.R;
import br.edu.unifei.bi20.kpi.KPI;
import br.edu.unifei.bi20.kpi.KPIAdapter;
import br.edu.unifei.bi20.kpi.KPIEntry;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.sql.SQLOutput;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

/**
 * Created by Bruno on 10/4/2015.
 */
public class JSONToKPI extends JSONToObject<KPIEntry> {
    public JSONToKPI(){
        super();
    }
    @Override
    public ArrayList<KPIEntry> processJSON(JSONObject object, Context c, String kpiname) throws ParseException {
        try {
            JSONArray rows = object.getJSONArray("rows");
            // Category[] data = new Category[rows.length()];
            ArrayList<KPIEntry> entries = new ArrayList<>();
            String[] meses_string = new String[120];
            Double[] meses_double = new Double[120];
            SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
            KPI kpi = new KPI();
            kpi.setIsFavorite(setPorcentage(kpiname));
            for (int r = 0; r < rows.length(); ++r) {
                JSONObject row = rows.getJSONObject(r);
                JSONArray columns = row.getJSONArray("c");
                if (r == 0) {
                    kpi.setName_kpi(kpiname);
                    kpi.setGood_threshold(stringToDouble(columns.getJSONObject(4).getString("v"), kpi));
                    kpi.setDanger_threshold(stringToDouble(columns.getJSONObject(5).getString("v"), kpi));

                }
                if(r == 7){
                    System.out.println("la");
                }
                Date d = sdf.parse("01/01/2001");
                String comment= "";
                    try {
                        meses_string[r] = columns.getJSONObject(2).getString("v");
                    }catch (Exception e){
                        meses_string[r] = "-";
                    }
                    String year,month,week;
                    year = columns.getJSONObject(1).getString("v");
                    month = columns.getJSONObject(0).getString("v");
                    comment = columns.getJSONObject(3).getString("v");
                    month = monthToNumber(month);
                    d= sdf.parse("15/"+month+"/"+year);
                    if(meses_string[r].contains("%")) meses_double[r]=0.01;
                    else meses_double[r] = 1.0;
                    meses_double[r] *= stringToDouble(meses_string[r],null);
                    // entries.add(entry);

                KPIEntry entry = new KPIEntry(kpi, meses_double[r]);
                entry.setDate(d);
                entry.setC(new Comments(comment,d));
                entries.add(entry);


            }


            return entries;

        } catch (JSONException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static ArrayList<KPIEntry> processJson2(JSONObject object) throws ParseException {
        //ArrayList<Category> items = new ArrayList<Category>();
        ArrayList<KPIEntry> entries= null;
        try {
            JSONArray rows = object.getJSONArray("rows");
            // Category[] data = new Category[rows.length()];
            entries = new ArrayList<>();

            SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");

            for (int r = 0; r < rows.length(); ++r) {
                JSONObject row = rows.getJSONObject(r);
                JSONArray columns = row.getJSONArray("c");
                String[] meses_string = new String[12];
                Double[] meses_double = new Double[12];
                Double month;
                int status = -1;
                String name = columns.getJSONObject(0).getString("v");
                String good_tresh= columns.getJSONObject(14).getString("v");
                String bad_tresh = columns.getJSONObject(15).getString("v");

                KPI kpi = new KPI();
                kpi.setName_kpi(name);
                if(good_tresh.contains("<")){
                    kpi.setGood_char('<');
                }else{
                    kpi.setGood_char('>');
                }
                if(bad_tresh.contains("<")){
                    kpi.setBad_char('<');
                }else{
                    kpi.setBad_char('>');
                }
                kpi.setGood_threshold(stringToDouble(good_tresh, kpi));
                kpi.setDanger_threshold(stringToDouble(bad_tresh, kpi));
                kpi.setIcon(KPI.getIconfromName(name));
                for(int i=0;i<12;++i){
                    meses_string[i] = columns.getJSONObject(i+1).getString("v");
                    if(meses_string[i].contains("%")) meses_double[i]=0.01;
                    else meses_double[i] = 1.0;
                    meses_double[i] *= stringToDouble(meses_string[i],null);
                    if(i == KPIAdapter.MONTH && meses_double[i] != -1f){
                        month = meses_double[i];
                        if(kpi.isPorcentage()) month= month*100;

                        if(kpi.getGood_char()=='<' && month < kpi.getGood_threshold()){
                            status = (R.mipmap.greendot);
                        }else if(kpi.getGood_char()=='>' && month >kpi.getGood_threshold()){
                            status = (R.mipmap.greendot);
                        }

                        if(kpi.getBad_char() == '<' && month<kpi.getDanger_threshold()){
                            status = (R.mipmap.reddot);
                        }else if(kpi.getBad_char() == '>' && month>kpi.getDanger_threshold()){
                            status = (R.mipmap.reddot);
                        }
                        if(status == -1) status = R.mipmap.yellowdot;
                        kpi.setStatus(status);
                    }
                }
                KPIEntry entry = new KPIEntry(kpi, meses_double[r]);
                entries.add(entry);

            }

        } catch (JSONException e) {
            e.printStackTrace();
        }
        return entries;
    }

    public static Double stringToDouble(String s, KPI k){
        Double ans = -1d;
        if(s.equalsIgnoreCase("-")){
            return ans;
        }else{
            if(s.contains("%") && k !=null){
                k.setIsPorcentage(true);

            }else{
                if(k!=null)
                    k.setIsPorcentage(false);
            }
            s = s.replace("%","").replace(">","").replace("<","").replace("=","");

            ans = Double.parseDouble(s);

        }


        return ans;
    }

    public static String monthToNumber(String month){
        month = month.toLowerCase();
        switch (month){
            case "janeiro":
                return "01";
            case "fevereiro":
                return "02";
            case "marco":
            case "março":
                return "03";
            case "abril":
                return "04";
            case "maio":
                return "05";
            case "junho":
                return "06";
            case "julho":
                return "07";
            case "agosto":
                return "08";
            case "setembro":
                return "09";
            case "outubro":
                return "10";
            case "novembro":
                return "11";
            case "dezembro":
                return "12";
            default:
                return "-1";
        }

    }

    public static boolean setPorcentage(String name){
        switch (name) {
            case "Back Order":
                return true;
            case "Days of Supply":
                return false;
            case "Deviation with Extension":
                return true;
            case "Deviation without Extension":
                return true;
            case "DFRFT":
                return true;
            case "Last Work Day Case":
                return false;
            case "Line Item Fill Rate":
                return true;
            case "On Time Release":
                return true;
            case "Schedule Attainment":
                return true;
            case "Total Cost":
                return false;
            case "Variable Cost":
                return false;
            case "Volume":
                return false;
            default:
                return false;
        }
    }



}
