package br.edu.unifei.bi20.kpi;

import java.util.Date;

import br.edu.unifei.bi20.Comments;

/**
 * Created by Bruno on 9/5/2015.
 */
public class KPIEntry {

    public KPI kpi;

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public Double getValue() {
        return value;
    }

    public void setValue(Double value) {
        this.value = value;
    }

    private Double value;
    private Date date;

    public Comments getC() {
        return c;
    }

    public void setC(Comments c) {
        this.c = c;
    }

    private Comments c;
    public KPIEntry(KPI name, Double value){
        this.kpi = name;
        this.value = value;
    }
}
