# README #

Repositório criado para o desenvolvimento do projeto BI 2.0 para o programa EPS - Embaixadores Johnson & Johnson em parceria com a Universidade Federal de Itajubá.

# MEMBROS #

* Bruno Fernandes      -  Engenharia da Computação '11
* João Gabriel Souza   -  Engenharia da Computação '12
* Rafael Amano         -  Engenharia Mecânica      '12
* Gabriel Bini Augusto -  Engenharia de Produção   '11
* Wanya Mendonça       -  Engenharia de Produção   '11
* Raíssa Cecília       -  Administração            '12
* Juliane Mari Higa    -  Administração            '10
* Paulo Silva Marra    -  Administração            '12


UNIFEI 2015